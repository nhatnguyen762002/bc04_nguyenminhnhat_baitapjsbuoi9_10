class NhanVien {
  constructor(acc, name, email, pass, date, salary, position, time) {
    this.acc = acc;
    this.name = name;
    this.email = email;
    this.pass = pass;
    this.date = date;
    this.salary = salary;
    this.position = position;
    this.time = time;
  }

  payRoll = function () {
    if (this.position == "Sếp") {
      return this.salary * 3;
    } else if (this.position == "Trưởng phòng") {
      return this.salary * 2;
    } else {
      return this.salary;
    }
  };
  classification = function () {
    if (this.time >= 192) {
      return "Xuất sắc";
    } else if (this.time >= 176) {
      return "Giỏi";
    } else if (this.time >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
