const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";
let dsnv = [];

let dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);

  for (let i = 0; i < dsnv.length; i++) {
    let nv = dsnv[i];

    dsnv[i] = new NhanVien(
      nv.acc,
      nv.name,
      nv.email,
      nv.pass,
      nv.date,
      nv.salary,
      nv.position,
      nv.time
    );
  }

  renderDSNV(dsnv);
}

document.getElementById("btnThem").addEventListener("click", function () {
  resetForm();
  document.querySelector("#btnCapNhat").disabled = true;
});

document.getElementById("btnThemNV").addEventListener("click", function () {
  let nhanVien = layThongTinTuForm();

  let isValid = kiemTraThongTin(nhanVien, dsnv);
  if (isValid) {
    dsnv.push(nhanVien);

    let dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);

    renderDSNV(dsnv);
  }
});

function xoaNhanVien(acc) {
  let indexNV = timKiemViTri(acc, dsnv);

  if (indexNV != -1) {
    dsnv.splice(indexNV, 1);

    let dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);

    renderDSNV(dsnv);
  }
}

function suaNhanVien(acc) {
  resetForm();

  document.querySelector("#btnThemNV").disabled = true;

  let indexNV = timKiemViTri(acc, dsnv);

  if (indexNV != -1) {
    let nv = dsnv[indexNV];
    showThongTinLenForm(nv);
  }
}

document.getElementById("btnCapNhat").addEventListener("click", function () {
  let newNhanVien = layThongTinTuForm();

  let isValid = kiemTraThongTinNotAcc(newNhanVien);
  if (isValid) {
    let indexNV = timKiemViTri(newNhanVien.acc, dsnv);

    dsnv[indexNV].name = newNhanVien.name;
    dsnv[indexNV].email = newNhanVien.email;
    dsnv[indexNV].pass = newNhanVien.pass;
    dsnv[indexNV].date = newNhanVien.date;
    dsnv[indexNV].salary = newNhanVien.salary;
    dsnv[indexNV].position = newNhanVien.position;
    dsnv[indexNV].time = newNhanVien.time;

    let dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);

    renderDSNV(dsnv);
  }
});

document.getElementById("btnTimNV").addEventListener("click", function () {
  let searchValue = document.getElementById("searchName").value;

  let searchValueLower = searchValue.toLowerCase();
  let dsnvFilter = dsnv.filter(function (nv) {
    let loaiNVLowerCase = nv.classification().toLowerCase();
    return loaiNVLowerCase.includes(searchValueLower);
  });

  if (dsnvFilter.length == 0) {
    document.querySelector("#tableDanhSach").innerHTML = `<tr>
    <span class="text-primary">Nhân viên loại <span class="text-danger">${searchValue}</span> không tồn tại!</span>
    </tr>`;
  } else {
    renderDSNV(dsnvFilter);
  }
});

document.getElementById("btnDong").addEventListener("click", function () {
  resetForm();
});
