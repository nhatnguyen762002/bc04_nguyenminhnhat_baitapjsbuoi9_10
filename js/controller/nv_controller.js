function layThongTinTuForm() {
  const accValue = document.querySelector("#tknv").value;
  const nameValue = document.querySelector("#name").value;
  const emailValue = document.querySelector("#email").value;
  const passValue = document.querySelector("#password").value;
  const dateValue = document.querySelector("#datepicker").value;
  const salaryValue = document.querySelector("#luongCB").value;
  const positionValue = document.querySelector("#chucvu").value;
  const timeValue = document.querySelector("#gioLam").value;

  return new NhanVien(
    accValue,
    nameValue,
    emailValue,
    passValue,
    dateValue,
    salaryValue,
    positionValue,
    timeValue
  );
}

function renderDSNV(nvArr) {
  var tbodyEl = document.querySelector("#tableDanhSach");
  tbodyEl.innerHTML = ``;

  nvArr.forEach(function (nv) {
    let { acc, name, email, pass, date, salary, position, time } = nv;

    tbodyEl.innerHTML += `<tr>
      <td>${acc}</td>
      <td>${name}</td>
      <td>${email}</td>
      <td>${date}</td>
      <td>${position}</td>
      <td>${new Intl.NumberFormat().format(nv.payRoll())}</td>
      <td>${nv.classification()}</td>
      <td>
      <button onclick="xoaNhanVien('${acc}')" class="btn btn-danger">Xóa</button>
      <button onclick="suaNhanVien('${acc}')" data-toggle="modal" data-target="#myModal" class="btn btn-warning">Sửa</button>
      </td>
      </tr>`;
  });
}

function resetForm() {
  document.querySelector("#tknv").value = ``;
  document.querySelector("#name").value = ``;
  document.querySelector("#email").value = ``;
  document.querySelector("#password").value = ``;
  document.querySelector("#datepicker").value = ``;
  document.querySelector("#luongCB").value = ``;
  document.querySelector("#chucvu").value = ``;
  document.querySelector("#gioLam").value = ``;

  document.querySelector("#tbTKNV").innerHTML = ``;
  document.querySelector("#tbTen").innerHTML = ``;
  document.querySelector("#tbEmail").innerHTML = ``;
  document.querySelector("#tbMatKhau").innerHTML = ``;
  document.querySelector("#tbNgay").innerHTML = ``;
  document.querySelector("#tbLuongCB").innerHTML = ``;
  document.querySelector("#tbChucVu").innerHTML = ``;
  document.querySelector("#tbGiolam").innerHTML = ``;

  document.querySelector("#tknv").disabled = false;
  document.querySelector("#btnThemNV").disabled = false;
  document.querySelector("#btnCapNhat").disabled = false;
}

function timKiemViTri(acc, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];

    if (nv.acc == acc) {
      return index;
    }
  }

  return -1;
}

function showThongTinLenForm(nv) {
  let { acc, name, email, pass, date, salary, position, time } = nv;

  document.querySelector("#tknv").value = acc;
  document.querySelector("#name").value = name;
  document.querySelector("#email").value = email;
  document.querySelector("#password").value = pass;
  document.querySelector("#datepicker").value = date;
  document.querySelector("#luongCB").value = salary;
  document.querySelector("#chucvu").value = position;
  document.querySelector("#gioLam").value = time;

  document.querySelector("#tknv").disabled = true;
}

function updateThongTinTuForm(dsnv) {
  const accValue = document.querySelector("#tknv").value;
  const nameValue = document.querySelector("#name").value;
  const emailValue = document.querySelector("#email").value;
  const passValue = document.querySelector("#password").value;
  const dateValue = document.querySelector("#datepicker").value;
  const salaryValue = document.querySelector("#luongCB").value;
  const positionValue = document.querySelector("#chucvu").value;
  const timeValue = document.querySelector("#gioLam").value;

  let indexNV = timKiemViTri(accValue, dsnv);

  dsnv[indexNV].name = nameValue;
  dsnv[indexNV].email = emailValue;
  dsnv[indexNV].pass = passValue;
  dsnv[indexNV].date = dateValue;
  dsnv[indexNV].salary = salaryValue;
  dsnv[indexNV].position = positionValue;
  dsnv[indexNV].time = timeValue;
}

function kiemTraThongTin(nv, dsnv) {
  let isValid =
    validator.kiemTraRong(nv.acc, "tbTKNV", "Tài khoản") &&
    validator.kiemTraNumber(nv.acc, "tbTKNV", "Tài khoản chỉ được nhập số!") &&
    validator.kiemTraDoDai(
      nv.acc,
      "tbTKNV",
      "Tài khoản phải 4 - 6 ký tự!",
      4,
      6
    ) &&
    validator.kiemTraAccTonTai(
      nv.acc,
      "tbTKNV",
      "Tài khoản đã tồn tại. Vui lòng nhập tài khoản khác!",
      dsnv
    );

  isValid =
    isValid &
    (validator.kiemTraRong(nv.name, "tbTen", "Tên nhân viên") &&
      validator.kiemTraChu(
        nv.name,
        "tbTen",
        "Tên nhân viên không được có số và ký tự đặc biệt!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.email, "tbEmail", "Email") &&
      validator.kiemTraEmail(nv.email, "tbEmail", "Email không hợp lệ!"));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.pass, "tbMatKhau", "Mật khẩu") &&
      validator.kiemTraMatKhauManh(
        nv.pass,
        "tbMatKhau",
        "Mật khẩu phải từ 6 - 10 ký tự. Chứa chữ hoa, chững thường, số và ký tự đặc biệt!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.date, "tbNgay", "Ngày làm") &&
      validator.kiemTraDate(
        nv.date,
        "tbNgay",
        "Vui lòng nhập ngày đúng định dạng! VD: 01/01/2000"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.salary, "tbLuongCB", "Lương cơ bản") &&
      validator.kiemTraNumber(
        nv.salary,
        "tbLuongCB",
        "Lương cơ bản chỉ được nhập số!"
      ) &&
      validator.kiemTra1MillionDen20Million(
        nv.salary,
        "tbLuongCB",
        "Lương cơ bản từ 1,000,000 - 20,000,000!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.position, "tbChucVu", "Chức vụ") &&
      validator.kiemTraChucVu(
        nv.position,
        "tbChucVu",
        "Vui lòng chọn một trong ba chức vụ (Sếp - Trưởng phòng - Nhân viên)!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.time, "tbGiolam", "Giờ làm") &&
      validator.kiemTraNumber(
        nv.time,
        "tbGiolam",
        "Giờ làm chỉ được nhập số!"
      ) &&
      validator.kiemTra80Den200(nv.time, "tbGiolam", "Giờ làm từ 80 - 200!"));

  return isValid;
}

function kiemTraThongTinNotAcc(nv) {
  let isValid =
    validator.kiemTraRong(nv.name, "tbTen", "Tên nhân viên") &&
    validator.kiemTraChu(
      nv.name,
      "tbTen",
      "Tên nhân viên không được có số và ký tự đặc biệt!"
    );

  isValid =
    isValid &
    (validator.kiemTraRong(nv.email, "tbEmail", "Email") &&
      validator.kiemTraEmail(nv.email, "tbEmail", "Email không hợp lệ!"));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.pass, "tbMatKhau", "Mật khẩu") &&
      validator.kiemTraMatKhauManh(
        nv.pass,
        "tbMatKhau",
        "Mật khẩu phải từ 6 - 10 ký tự. Chứa chữ hoa, chững thường, số và ký tự đặc biệt!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.date, "tbNgay", "Ngày làm") &&
      validator.kiemTraDate(
        nv.date,
        "tbNgay",
        "Vui lòng nhập ngày đúng định dạng! VD: 01/01/2000"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.salary, "tbLuongCB", "Lương cơ bản") &&
      validator.kiemTraNumber(
        nv.salary,
        "tbLuongCB",
        "Lương cơ bản chỉ được nhập số!"
      ) &&
      validator.kiemTra1MillionDen20Million(
        nv.salary,
        "tbLuongCB",
        "Lương cơ bản từ 1,000,000 - 20,000,000!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.position, "tbChucVu", "Chức vụ") &&
      validator.kiemTraChucVu(
        nv.position,
        "tbChucVu",
        "Vui lòng chọn một trong ba chức vụ (Sếp - Trưởng phòng - Nhân viên)!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(nv.time, "tbGiolam", "Giờ làm") &&
      validator.kiemTraNumber(
        nv.time,
        "tbGiolam",
        "Giờ làm chỉ được nhập số!"
      ) &&
      validator.kiemTra80Den200(nv.time, "tbGiolam", "Giờ làm từ 80 - 200!"));

  return isValid;
}
