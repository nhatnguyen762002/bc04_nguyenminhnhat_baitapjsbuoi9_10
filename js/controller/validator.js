let validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(
        idError
      ).innerText = `${message} không được để rỗng!`;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (re.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  kiemTraNumber: function (value, idError, message) {
    var reg = /^\d+$/;

    if (reg.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  kiemTraChu: function (value, idError, message) {
    var reg =
      /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ]+$/;

    if (reg.test(value.replace(/\s/g, ""))) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  kiemTraMatKhauManh: function (value, idError, message) {
    const isStrongPassword =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{6,10}$/;

    if (isStrongPassword.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  // kiemTra0Den10: function (value, idError, message) {
  //   var point = /^([0-9]|10)$/;

  //   if (point.test(value)) {
  //     document.getElementById(idError).innerText = "";
  //     return true;
  //   } else {
  //     document.getElementById(idError).style.display = "flex";
  //     document.getElementById(idError).innerText = message;
  //     return false;
  //   }
  // },

  kiemTraDate: function (value, idError, message) {
    let re = /^[0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4}$/g;

    if (re.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  kiemTra1MillionDen20Million: function (value, idError, message) {
    var point = /^([1-9]\d{6}|1\d{7}|20{7})$/;

    if (point.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  kiemTra80Den200: function (value, idError, message) {
    var point = /^([8-9]\d{1}|1\d{2}|200)$/;

    if (point.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  kiemTraAccTonTai: function (value, idError, message, dsnv) {
    var dsMaNV = dsnv.map(function (nv) {
      return nv.acc;
    });

    if (dsMaNV.includes(value)) {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraChucVu: function (value, idError, message) {
    if (value == "Sếp" || value == "Trưởng phòng" || value == "Nhân viên") {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).style.display = "flex";
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
};
